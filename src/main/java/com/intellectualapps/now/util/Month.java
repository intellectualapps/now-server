/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.now.util;

/**
 *
 * @author Lateefah
 */
public class Month {
    
    private Double January;
    private Double February;
    private Double March;
    private Double April;
    private Double May;
    private Double June;
    private Double July;
    private Double August;
    private Double September;
    private Double October;
    private Double November;
    private Double December;

    public Double getJanuary() {
        return January;
    }

    public void setJanuary(Double January) {
        this.January = January;
    }

    public Double getFebruary() {
        return February;
    }

    public void setFebruary(Double February) {
        this.February = February;
    }

    public Double getMarch() {
        return March;
    }

    public void setMarch(Double March) {
        this.March = March;
    }

    public Double getApril() {
        return April;
    }

    public void setApril(Double April) {
        this.April = April;
    }

    public Double getMay() {
        return May;
    }

    public void setMay(Double May) {
        this.May = May;
    }

    public Double getJune() {
        return June;
    }

    public void setJune(Double June) {
        this.June = June;
    }

    public Double getJuly() {
        return July;
    }

    public void setJuly(Double July) {
        this.July = July;
    }

    public Double getAugust() {
        return August;
    }

    public void setAugust(Double August) {
        this.August = August;
    }

    public Double getSeptember() {
        return September;
    }

    public void setSeptember(Double September) {
        this.September = September;
    }

    public Double getOctober() {
        return October;
    }

    public void setOctober(Double October) {
        this.October = October;
    }

    public Double getNovember() {
        return November;
    }

    public void setNovember(Double November) {
        this.November = November;
    }

    public Double getDecember() {
        return December;
    }

    public void setDecember(Double December) {
        this.December = December;
    }

    
}

