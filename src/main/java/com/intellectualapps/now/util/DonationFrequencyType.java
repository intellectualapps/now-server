/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.now.util;

/**
 *
 * @author buls
 */
public enum DonationFrequencyType {
    
    STOP(-1), MANUAL(0), WEEKLY(1), DAILY(2),
    MONTHLY(3), ANNUAL(4);
    
    Integer value;

    DonationFrequencyType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value.toString();
    }

}

